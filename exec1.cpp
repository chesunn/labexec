#include <iostream>
#include <cmath>
using namespace std;

int main()
{
	int nm;
	cout << "Enter a number:" << endl;
	cin >> nm;
	for (int i=2;i<6;i++) {
		cout << nm << "^" << i << " = " << pow(nm, i) << endl;
	}
	return 0;
}
