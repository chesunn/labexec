#include <iostream>
#include <limits>
using namespace std;

int main() {
	int a, b, c;
	b = numeric_limits<int>::min(); c = numeric_limits<int>::max();
	while(cin>>a) {
		b = max(b, a);
		c = min(c, a);
	};
	cout << "The highest number is " << b
	<< " and the lowest is " << c << endl;
	return 0;
}
