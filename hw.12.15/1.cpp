/***
FN:F79708
PID:1
GID:1
*/

#include <iostream>

using namespace std;

double numbers_sum(int howMany) {
    double a, b = 0;
    for (int i=0; i<howMany; i++) {
        cin >> a;
        b += a;
    }
    return b;
}

int main()
{
	int number;
	cout << "Enter a number:" << endl;
	cin >> number;
	cout << "The sum is: " << numbers_sum(number) << endl;
	return 0;
}
