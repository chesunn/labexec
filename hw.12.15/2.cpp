/***
FN:F79708
PID:2
GID:1
*/

#include <iostream>
#include <cmath>
using namespace std;

//functions here
int smallestDigit(int number) {
    int count = 1;
    int rep = number;
    while (rep > 9) {
        count++;
        rep = rep/10;
    }
    int a, c, d, e;
    c = 9;
    for (int i = 1; i<=count; i++) {         //Компилатора ме съдра от грешки ( % pow(x,y) )
        d = pow(10, i-1);
        e = pow(10, i);                     // И на мен не ми харесва как стана,
        a = (number % e) - (number % d);    // но работи..
        a = a / pow(10, i-1);
        if ( a < c)
            c = a;
    }
    return c;
}

int main()
{
	int number;
	cout << "Enter a number:" << endl;
	cin >> number;
	cout << "The smallest digit is : " << smallestDigit(number) << endl;
	return 0;
}
