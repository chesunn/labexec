/***
FN:F79708
PID:3
GID:1
*/

#include <iostream>

using namespace std;

void studentStatus ( string &e, double score) {
    if (score > 6 || score < 2)
        e = e + '\t' + "[INVALID]";
    else if (score >= 3)
        e = e + '\t' + "[PASS]";
    else
        e = e + '\t' + "[FAIL]";
}

int main()
{
	string fnm;
	double score;
	cout << "Enter faculty number followed by score" << endl;
	while (cin >> fnm && cin >> score) {
        studentStatus(fnm, score);
        cout << fnm;
	}
	return 0;
}
