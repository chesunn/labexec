#include <iostream>
using namespace std;

int main()
 {
	double size;
	double eff;
	cout << "Enter a tank size:" << endl;
	cin >> size;
	cout << "Enter fuel efficiency (km per liter):" << endl;
	cin >> eff;
	double price;
	cout << "Enter price per liter:" << endl;
	cin >> price;
	double oneh = 100 / eff;
	cout << "We can travel " << eff*size << "km." << endl;
	cout << "For every 100km it will cost " << oneh*price << "lv." << endl; 
	return 0;
}
