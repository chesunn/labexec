#include <iostream>
using namespace std;

int main() {
	int a;
	int b;
	int c;
	cout << "Enter three numbers:" << endl;
	cin >> a >> b >> c;
	if ( a == b || a == c ) {
		cout << "The are duplicate numbers - " << a << endl;
	}
	else if ( b == c ) {
		cout << "The are duplicate numbers - " << b << endl;
	}
	else cout << "There are no duplicates" << endl;
	return 0;
}
