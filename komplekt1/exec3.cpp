#include <iostream>
using namespace std;

int main() {
	string oper;
	string operant = "asmp";
	double a;
	int b;
	cout << "Enter operation(a/s/m/p):" << endl;
	cin >> oper;
	int pos = operant.find(oper);
	cout << "Enter operand 1:" << endl;
	cin >> a;
	cout << "Enter operand 2:" << endl;
	cin >> b;
	switch (pos) {
		case 0:
			cout << a << " + " << b << " is " << a+b << endl;
			break;
		case 1:
			cout << a << " - " << b << " is " << a-b << endl;
			break;
		case 2:
			cout << a << " * " << b << " is " << a*b << endl;
			break;
		case 3:
			cout << a << " / " << b << " is " << a/b << endl;
			break;
		default:
			cout << "Invalid operation!" << endl;
		}
	return 0;
}
