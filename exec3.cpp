#include <iostream>
using namespace std;

int main()
{
	cout << "Enter a number in meters:" << endl;
	double a;
	cin >> a;
	cout << a << " m = " << a / 1000 << " km" << endl;
	cout << a << " m = " << a * 10 << " dm" << endl;
	cout << a << " m = " << a * 100 << " cm" << endl;
	return 0;
}
