#include <iostream>
using namespace std;

int main() {
	double nm1;
	double nm2;
	cout << "Enter number 1:" << endl;
	cin >> nm1;
	cout << "Enter number 2:" << endl;
	cin >> nm2;
	if ( nm1 > nm2 ) {
		cout << "The larger number is " << nm1 << endl;
	}
	else if ( nm1 == nm2 ) {
		cout << "The numbers are equal." << endl;
	}
	else {
		cout << "The larger number is " << nm2 << endl;
	}
	return 0;
}
