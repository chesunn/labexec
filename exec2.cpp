#include <iostream>
using namespace std;

int main() 
{	
	double a;
	double b;
	cout << "Enter number a:" << endl;
	cin >> a;
	cout << "Enter number b:" << endl;
	cin >> b;
	cout << "a + b = " << a+b << endl;
	cout << "a - b = " << a-b << endl;
	cout << "a * b = " << a*b << endl;
	cout << "a / b = " << a / b << endl;
	return 0;
}
